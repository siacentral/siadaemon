package siadaemon

import (
	"bufio"
	cryptoRand "crypto/rand"
	"encoding/base64"
	"io"
	"strings"
	"time"
)

func randomBytes(n int) (bytes []byte, err error) {
	bytes = make([]byte, n)

	_, err = cryptoRand.Read(bytes)

	return
}

func randomKey(n int) (key string, err error) {
	keyBytes, err := randomBytes(n)

	if err != nil {
		return
	}

	key = base64.StdEncoding.EncodeToString(keyBytes)

	return
}

func cleanString(str string) string {
	return strings.Map(func(r rune) rune {
		if r >= 32 && r != 127 {
			return r
		}

		return -1
	}, str)
}

func buildSiaArgs(config SiadConfig) (args []string) {
	if len(config.DataPath) > 0 {
		args = append(args, []string{"-d", config.DataPath}...)
	}

	if len(config.APIAgent) > 0 {
		args = append(args, []string{"--agent", config.APIAgent}...)
	}

	if len(config.HostPort) > 0 {
		args = append(args, []string{"--host-addr", config.HostPort}...)
	}

	if len(config.APIAddr) > 0 {
		args = append(args, []string{"--api-addr", config.APIAddr}...)
	}

	if len(config.RPCPort) > 0 {
		args = append(args, []string{"--rpc-addr", config.RPCPort}...)
	}

	if len(config.Modules) > 0 {
		args = append(args, []string{"-M", config.Modules}...)
	}

	return
}

func stdoutReader(reader io.ReadCloser) {
	in := bufio.NewScanner(reader)

	for in.Scan() {
		text := cleanString(in.Text())

		if strings.HasPrefix(strings.ToLower(text), "finished loading in") {
			siaFullyLoaded = true
			firstLoadComplete = true
			notify("sia fully loaded", NotifyLoad)
		}

		addStdOut(text, PID())

		notify(text, NotifyStdErr)
	}
}

func stderrReader(reader io.ReadCloser) {
	in := bufio.NewScanner(reader)

	for in.Scan() {
		text := cleanString(in.Text())

		addErrOut(text, PID())

		notify(text, NotifyStdErr)
	}
}

func blockUntil(flags ...NotifierFlag) Notifier {
	wait := make(chan Notifier, 1)

	RegisterNotifier(wait, flags...)

	defer UnregisterNotifier(wait)

	return <-wait
}

func blockUntilTimeout(timeout time.Duration, flags ...NotifierFlag) (Notifier, error) {
	wait := make(chan Notifier, 1)

	RegisterNotifier(wait, flags...)

	defer UnregisterNotifier(wait)

	select {
	case notification := <-wait:
		return notification, nil
	case <-time.After(timeout):
		return Notifier{}, ErrTimeout
	}
}
