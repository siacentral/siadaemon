package siadaemon

import "errors"

var (
	//ErrAlreadyRunning occurs when Start is called before siad has fully shut down
	ErrAlreadyRunning = errors.New("siad already running")

	//ErrTimeout occurs when the block timeout has elapsed
	ErrTimeout = errors.New("timeout elapsed")

	//ErrNotRunning occurs when siad is not running
	ErrNotRunning = errors.New("siad not running")

	//ErrFailedToLoad occurs when siad crashes but never fully loaded at least once
	ErrFailedToLoad = errors.New("siad failed to load the first time")
)
